package io.gitlab.jumperdenfer.bonsaitreejp.bonsai;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.Tag;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.world.ChunkLoadEvent;

import io.gitlab.jumperdenfer.bonsaitreejp.BonsaiTreeJP;

public class ChunckCheck implements Listener{
	private BonsaiTreeJP bonsaiTree;
	private List<BonsaiPot> bonsaiPots = new ArrayList<BonsaiPot>();
	private Chunk chunk;
	
	
	public ChunckCheck(BonsaiTreeJP bonsaiTree) {
		this.bonsaiTree = bonsaiTree;
	}
	
	
	
	@EventHandler
	public void onChunkLoad(ChunkLoadEvent chunkLoad) {
		this.chunk = chunkLoad.getChunk();
		this.getBonsaiPot();
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent playerMove) {
		this.chunk = playerMove.getPlayer().getLocation().getChunk();
		this.getBonsaiPot();
	}
	
	@EventHandler()
	public void onBreakBonsai(BlockBreakEvent blockevent) {
		for(BonsaiPot bonsai : bonsaiPots) {
    		if(bonsai.getLocation().equals(blockevent.getBlock().getLocation())) {
    			Bukkit.getScheduler().cancelTask(bonsai.taskRunnerId);
    			int indexToRemove = bonsaiPots.indexOf(bonsai);
    			this.bonsaiPots.remove(indexToRemove);
    			break;
    		}
    	}
	}
	
	
	private void getBonsaiPot() {
			for(int x=0; x<16; x++) {
				for(int y=50; y<70; y++) {
					for(int z=0; z<16; z++) {
						Block block = this.chunk.getBlock(x, y, z);
	                	Material material = block.getType();
	                    if(Tag.FLOWER_POTS.isTagged(material)) {
	                    	boolean alreadyBonsai = false;
	                    	for(BonsaiPot bonsai : bonsaiPots) {
	                    		if(bonsai.getLocation().equals(block.getLocation()) && bonsai.type != block.getType()) {
	                    			Bukkit.getScheduler().cancelTask(bonsai.taskRunnerId);
	                    			int indexToRemove = bonsaiPots.indexOf(bonsai);
	                    			this.bonsaiPots.remove(indexToRemove);
	                    			break;
	                    		}
	                    		else if(bonsai.getLocation().equals(block.getLocation())) {
	                    			alreadyBonsai = true;
	                    		}
	                    	}
	                    	if(!alreadyBonsai) {

		                    	this.bonsaiPots.add(new BonsaiPot(bonsaiTree,block));
	                    	}
	                    }
					}
				}
	                
			}
	}
}
