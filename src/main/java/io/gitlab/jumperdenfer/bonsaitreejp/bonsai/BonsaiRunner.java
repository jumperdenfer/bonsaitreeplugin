package io.gitlab.jumperdenfer.bonsaitreejp.bonsai;

import org.bukkit.Bukkit;

public class BonsaiRunner implements Runnable{
	private BonsaiPot bonsai;
	
	public BonsaiRunner(BonsaiPot bonsai) {
		this.bonsai = bonsai;
	}
	
	public void run() {
		try {
			if(bonsai.isBonsai && bonsai.bonsaiBlock.getChunk().isLoaded()) {
				this.bonsai.generateResources();
			}
			
		}catch(Exception e) {
			Bukkit.getLogger().info(e.getMessage());
		}
		
	}

}
