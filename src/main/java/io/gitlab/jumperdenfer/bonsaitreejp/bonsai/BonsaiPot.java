package io.gitlab.jumperdenfer.bonsaitreejp.bonsai;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Tag;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Container;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import io.gitlab.jumperdenfer.bonsaitreejp.BonsaiTreeJP;

public class BonsaiPot{
	public boolean isBonsai = true;
	public Block bonsaiBlock;
	public Material type;
	private List<ItemStack> resourceProduced = new ArrayList<ItemStack>();
	private Inventory containerInventory;
	public int taskRunnerId = -1;
	
	public BonsaiPot(BonsaiTreeJP bonsaiTreeJP,Block bonsaiBlock) {
		this.bonsaiBlock = bonsaiBlock;
		this.initialize();
		if(this.isBonsai){
			this.taskRunnerId = Bukkit.getScheduler().runTaskTimer(bonsaiTreeJP, new BonsaiRunner(this), 100, 100).getTaskId();
		}
	}
	
	public Location getLocation() {
		return this.bonsaiBlock.getLocation();
	}
	
	public void generateResources() {
		int maxIndex = this.resourceProduced.size();
		int indexRandom = this.random(0, maxIndex);
		this.checkExist();
		if(!(containerInventory.firstEmpty() == -1) && this.isBonsai) {
			containerInventory.addItem(this.resourceProduced.get(indexRandom));
		}
		else {
			this.isBonsai = false;
			Bukkit.getScheduler().cancelTask(taskRunnerId);
		}
	}
		
	private void initialize() {
		if(Tag.FLOWER_POTS.isTagged(bonsaiBlock.getType())) {
			Block belowContainer = this.bonsaiBlock.getRelative(BlockFace.DOWN);
			BlockState belowContainerState = belowContainer.getState();
			if(!(belowContainerState instanceof Container)) {
				this.isBonsai = false;
			}
			Container container = ((Container) belowContainerState);
			this.containerInventory = container.getInventory();
			this.type = this.bonsaiBlock.getState().getType();
			this.setUpResource();
		}
	}
	
	private void checkExist() {
		Block block = this.bonsaiBlock;
		if(this.bonsaiBlock.getType() == Material.FLOWER_POT
			|| block.isEmpty()
			|| this.type != this.bonsaiBlock.getLocation().getBlock().getType()) {
			this.isBonsai = false;
		}
		else if(block.getRelative(BlockFace.DOWN).getType() == Material.AIR) {
			this.isBonsai = false;
		}
	}
	
	private void setUpResource() {
		switch(this.type) {
			case POTTED_ACACIA_SAPLING:
				this.noVariantResource();
				break;
			case POTTED_OAK_SAPLING:
				this.OakResource();
				break;
			case POTTED_SPRUCE_SAPLING:
				this.noVariantResource();
				break;
			case POTTED_DARK_OAK_SAPLING:
				this.noVariantResource();
				break;
			case POTTED_BIRCH_SAPLING:
				this.noVariantResource();
				break;
			case POTTED_JUNGLE_SAPLING:
				this.JungleResource();
				break;
			default:
				this.isBonsai = false;
				break;
		}
	}
	
	
	private void noVariantResource() {
		this.stickGenerator();
		this.leaveGenerator();
		this.logGenerator();
	}
	
	private void OakResource() {
		this.noVariantResource();
		this.appleGenerator();
	}
	private void JungleResource() {
		this.noVariantResource();
		this.cacaoGenerator();
	}
	
	
	
	private void appleGenerator() {
		Material apple = Material.APPLE;
		ItemStack appleStack = new ItemStack(apple,this.random(1, 2));
		this.resourceProduced.add(appleStack);
	}
	
	private void stickGenerator() {
		Material stick = Material.STICK;
		ItemStack stickStack = new ItemStack(stick,this.random(2, 4));
		this.resourceProduced.add(stickStack);
	}
	
	private void leaveGenerator() {
		Material typeOfLeave;
		switch(this.type) {
			case POTTED_ACACIA_SAPLING:
				typeOfLeave = Material.ACACIA_LEAVES;
				break;
			case POTTED_OAK_SAPLING:
				typeOfLeave = Material.OAK_LEAVES;
				break;
			case POTTED_SPRUCE_SAPLING:
				typeOfLeave = Material.SPRUCE_LEAVES;
				break;
			case POTTED_DARK_OAK_SAPLING:
				typeOfLeave = Material.DARK_OAK_LEAVES;
				break;
			case POTTED_BIRCH_SAPLING:
				typeOfLeave = Material.BIRCH_SAPLING;
				break;
			case POTTED_JUNGLE_SAPLING:
				typeOfLeave = Material.JUNGLE_SAPLING;
				break;
			default:
				typeOfLeave = Material.OAK_LEAVES;
				break;
		}
		ItemStack leaveStack = new ItemStack(typeOfLeave,this.random(1, 3));
		this.resourceProduced.add(leaveStack);
	}
	
	private void logGenerator() {
		Material typeOfWood;
		switch(this.type) {
			case POTTED_ACACIA_SAPLING:
				typeOfWood = Material.ACACIA_LOG;
				break;
			case POTTED_OAK_SAPLING:
				typeOfWood = Material.OAK_LOG;
				break;
			case POTTED_SPRUCE_SAPLING:
				typeOfWood = Material.SPRUCE_LOG;
				break;
			case POTTED_DARK_OAK_SAPLING:
				typeOfWood = Material.DARK_OAK_LOG;
				break;
			case POTTED_BIRCH_SAPLING:
				typeOfWood = Material.BIRCH_LOG;
				break;
			case POTTED_JUNGLE_SAPLING:
				typeOfWood = Material.JUNGLE_LOG;
				break;
			default:
				typeOfWood = Material.OAK_LOG;
				break;
		}
		ItemStack woodLog = new ItemStack(typeOfWood,this.random(1,3));
		this.resourceProduced.add(woodLog);
	}

	private void cacaoGenerator() {
		Material cacao = Material.COCOA_BEANS;
		ItemStack cacaoStack = new ItemStack(cacao,this.random(1, 2));
		this.resourceProduced.add(cacaoStack);
	}
	
	
	private int random(int min,int max) {
		return (int) Math.floor(Math.random() * max) + min;
	}
}
