package io.gitlab.jumperdenfer.bonsaitreejp;

import org.bukkit.plugin.java.JavaPlugin;

import io.gitlab.jumperdenfer.bonsaitreejp.bonsai.ChunckCheck;

public class BonsaiTreeJP extends JavaPlugin{

	@Override
	public void onEnable(){
		this.getServer().getPluginManager().registerEvents(new ChunckCheck(this), this);
		getLogger().info("BonsaiTree Enabled");
	}
	
	public void onDisable() {
		getLogger().info("BonsaiTree Disabled");
	}
}
